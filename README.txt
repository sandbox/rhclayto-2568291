Description
-----------
Expires rows from the MongoDB Sessions session table older than a certain time. This module is only useful if you use the MongoDB Drupal module to store sessions.

On busy sites, the sessions table can grow to be very large, and that can
cause slow accesses to it, as well as slow writes due to locking, leading
to performance bottlenecks.

By trimming the table regularly, the above bottlenecks are avoided.

Drupal uses the PHP garbage collection mechanism to clean up the sessions
table, but this mechanism depends on PHP's configuration, and can fire for
any session.

This module moves this functionality to cron, and hence is a background
process, and is consistent and predictable regardless of PHP's garbage
collection configuration.

Installation
------------
To install, copy the module's directory and all its contents to your modules
directory, normally sites/all/modules.

Configuration
-------------
To enable this module, visit Administer -> Site building -> Modules.

To configure it, go to Administer -> Site configuration -> Session Expire Mongo.

The default settings are suitable for most sites, but you can adjust them
to your particular needs. The instructions there should be self-explanatory.

Authors
------
Khalid Baheyeldin (http://baheyeldin.com/khalid and http://2bits.com) wrote the original Session Expire module on which this is based (http://drupal.org/project/session_expire). Most of the code is his. Henry Clayton tweaked it to work with the MongoDB session backend.
